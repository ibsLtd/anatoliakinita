<?php

/*** Child Theme Function  ***/

function anatoliakinita_child_theme_enqueue_scripts()
{

    if (!is_admin()) {
        wp_enqueue_style('parent-theme', get_template_directory_uri() . '/style.css');
        wp_enqueue_style('anatoliakinita-css', get_stylesheet_directory_uri() . '/public/style.css');
    }
}

add_action('wp_enqueue_scripts', 'anatoliakinita_child_theme_enqueue_scripts');


/**
 * Function to defer all scripts which are not excluded
 */
function anatoliakinita_js_defer_attr($tag)
{
    if (is_admin()) {
        return $tag;
    }

    $scripts_to_exclude = array();

    foreach ($scripts_to_exclude as $exclude_script) {
        if (true == strpos($tag, $exclude_script))
            return $tag;
    }

    return str_replace(' src', ' defer src', $tag);
}

add_filter('script_loader_tag', 'anatoliakinita_js_defer_attr', 10);

/**
 * Remove junk from head
 */
// remove WordPress version number
function anatoliakinita_remove_version()
{
    return '';
}

add_filter('the_generator', 'anatoliakinita_remove_version');
remove_action('wp_head', 'wp_generator');

remove_action('wp_head', 'rsd_link'); // remove really simple discovery (RSD) link
remove_action('wp_head', 'wlwmanifest_link'); // remove wlwmanifest.xml (needed to support windows live writer)

remove_action('wp_head', 'feed_links', 2); // remove rss feed links (if you don't use rss)
remove_action('wp_head', 'feed_links_extra', 3); // removes all extra rss feed links

remove_action('wp_head', 'index_rel_link'); // remove link to index page

remove_action('wp_head', 'start_post_rel_link', 10, 0); // remove random post link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // remove parent post link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // remove the next and previous post links
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0); // remove shortlink

// disable the admin bar
add_filter('show_admin_bar', '__return_false');

/**
 * Disable the emoji's
 */
function anatoliakinita_disable_emojis()
{
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    add_filter('tiny_mce_plugins', 'anatoliakinita_disable_emojis_tinymce');
    add_filter('wp_resource_hints', 'anatoliakinita_disable_emojis_remove_dns_prefetch', 10, 2);
}

add_action('init', 'anatoliakinita_disable_emojis');

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 * @return array Difference betwen the two arrays
 */
function anatoliakinita_disable_emojis_tinymce($plugins)
{
    if (is_array($plugins)) {
        return array_diff($plugins, array('wpemoji'));
    } else {
        return array();
    }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function anatoliakinita_disable_emojis_remove_dns_prefetch($urls, $relation_type)
{
    if ('dns-prefetch' == $relation_type) {
        /** This filter is documented in wp-includes/formatting.php */
        $emoji_svg_url = apply_filters('emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/');
        $urls = array_diff($urls, array($emoji_svg_url));
    }
    return $urls;
}

/**
 * Disable embeds
 */
function anatoliakinita_disable_embeds()
{

    // Remove the REST API endpoint.
    remove_action('rest_api_init', 'wp_oembed_register_route');

    // Turn off oEmbed auto discovery.
    add_filter('embed_oembed_discover', '__return_false');

    // Don't filter oEmbed results.
    remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

    // Remove oEmbed discovery links.
    remove_action('wp_head', 'wp_oembed_add_discovery_links');

    // Remove oEmbed-specific JavaScript from the front-end and back-end.
    remove_action('wp_head', 'wp_oembed_add_host_js');
    add_filter('tiny_mce_plugins', 'anatoliakinita_disable_embeds_tiny_mce_plugin');

    // Remove all embeds rewrite rules.
    add_filter('rewrite_rules_array', 'anatoliakinita_disable_embeds_rewrites');

    // Remove filter of the oEmbed result before any HTTP requests are made.
    remove_filter('pre_oembed_result', 'wp_filter_pre_oembed_result', 10);
}

add_action('init', 'anatoliakinita_disable_embeds', 9999);

function anatoliakinita_disable_embeds_tiny_mce_plugin($plugins)
{
    return array_diff($plugins, array('wpembed'));
}

function anatoliakinita_disable_embeds_rewrites($rules)
{
    foreach ($rules as $rule => $rewrite) {
        if (false !== strpos($rewrite, 'embed=true')) {
            unset($rules[$rule]);
        }
    }
    return $rules;
}

/**
 * Remove query strings
 */
function anatoliakinita_remove_script_version($src)
{
    $parts = explode('?ver', $src);
    return $parts[0];
}

add_filter('script_loader_src', 'anatoliakinita_remove_script_version', 15, 1);
add_filter('style_loader_src', 'anatoliakinita_remove_script_version', 15, 1);




function remove_menus()
{


    remove_menu_page('edit-comments.php');          //Comments
    remove_menu_page('tools.php');                  //Tools
    remove_menu_page( 'edit.php?post_type=project' );    //Projects
}

add_action('admin_menu', 'remove_menus');


function remove_user_css()
{
    echo '<style type="text/css">
            #postcustom.postbox ,
            #postexcerpt.postbox{
                display:none;
            }
         </style>';
}

add_action('admin_head-user-edit.php', 'remove_user_css');
add_action('admin_head-profile.php', 'remove_user_css');

function recent_posts($args)
{
    $url = $_SERVER['REQUEST_URI'];
    $readMore = 'Διαβάστε περισσότερα';
    if($url === '/en/'){
        $readMore = 'Read more';
    }
    // get the posts
    $posts = get_posts(
        array(
            'numberposts' => 2,
            "suppress_filters" => false
        )
    );

    // No posts? run away!
    if (empty($posts)) return '';

    $out = '<ul class="ibs-recent-posts">';
    foreach ($posts as $post) {
        $excerpt = strip_tags($post->post_content);
        if (strlen($excerpt) > 10) {
            $excerpt = substr($excerpt, 0, 100);
            $excerpt = substr($excerpt, 0, strrpos($excerpt, ' '));
            $excerpt .= '...';
        }
        $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'thumbnail');
        $out .= '<li><div class="img-container"><img src= ' . $image[0] . '></div><div class="post-details"><span>' . $post->post_title . '</span><p>' . $excerpt . '</p><a  class="read-more" href=' . get_permalink($post->ID) . '>'.$readMore.'</a></div></li>';
    }
    $out .= '</ul>';
    return $out;
}

add_shortcode('RecentPosts', 'recent_posts');
